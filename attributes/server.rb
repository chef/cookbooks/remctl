default['remctl']['acl_files'] = {}
default['remctl']['clean'] = false
default['remctl']['command_files'] = {}
default['remctl']['command_dirs'] = {}
default['remctl']['include_files'] = []
default['remctl']['init'] = if platform_family?('debian')
                              'inetd'
                            elsif platform_family?('rhel')
                              'systemd'
                            end
