#
# Cookbook:: remctl
# Recipe:: systemd
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

edit_resource(:service, 'remctld')

find_resource(:execute, 'systemctl daemon-reload') do
  command 'systemctl daemon-reload'
  action :nothing
end

if platform_family?('debian')
  %w(service socket).each do |type|
    file "/lib/systemd/system/remctld.#{type}" do
      if remctl_use_systemd?
        content(lazy do # remctl-server has to be installed first
          IO.read("/usr/share/doc/remctl-server/examples/remctld.#{type}")
        end)
        user 'root'
        group 'root'
        mode '0644'
        backup false
        only_if do
          ::File.exist?("/usr/share/doc/remctl-server/examples/remctld.#{type}") &&
            !::File.exist?("/lib/systemd/system/remctld.#{type}")
        end
        notifies :run, 'execute[systemctl daemon-reload]'
        notifies :enable, 'service[remctld]'
        notifies :restart, 'service[remctld]'
      else
        action :delete
        notifies :stop, 'service[remctld]', :immediately
        notifies :run, 'execute[systemctl daemon-reload]'
      end
    end
  end
else
  edit_resource(:service, 'remctld') do
    action %i(enable start) if remctl_use_systemd?
    action :stop unless remctl_use_systemd?
  end
end
