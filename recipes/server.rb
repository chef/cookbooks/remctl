#
# Cookbook:: remctl
# Recipe:: server
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe 'remctl::client'

if platform_family?('debian')
  package 'remctl-server'
elsif platform_family?('rhel')
  # packages were already installed by remctl::client
else
  raise 'Unsupported platform family'
end

# Create directories
['/etc/remctl',
 '/etc/remctl/acl',
 '/etc/remctl/conf.d'].each do |dir|
  directory dir do
    owner 'root'
    group 'root'
    mode '0755'
  end
end

# Update main configuration file
template remctl_config_path do
  source 'remctl.conf.erb'
  variables(include_files: node['remctl']['include_files'] || [])
  notifies remctl_inetd_restart_action, 'service[inetd]' if remctl_use_inetd?
  notifies :restart, 'service[remctld]' unless remctl_use_inetd?
end

# Check for a valid init type
if platform?('debian') && node['platform_version'].to_i >= 10 && remctl_use_systemv?
  Chef::Log.warn 'No support for System-V on Debian >= 10, using inetd'
  node.override['remctl']['init'] = 'inetd'
elsif platform_family?('rhel') && remctl_use_inetd?
  Chef::Log.warn 'No support for inetd on RHEL, using systemd'
  node.override['remctl']['init'] = 'systemd'
elsif platform_family?('rhel') && remctl_use_systemv?
  Chef::Log.warn 'No support for System-V on RHEL, using systemd'
  node.override['remctl']['init'] = 'systemd'
end

# Include the wanted recipe last, so that the others can clean up first
if remctl_use_inetd?
  include_recipe 'remctl::systemd'
  include_recipe 'remctl::systemv' if platform_family?('debian')
  include_recipe 'remctl::inetd'
elsif remctl_use_systemd?
  include_recipe 'remctl::inetd'
  include_recipe 'remctl::systemv' if platform_family?('debian')
  include_recipe 'remctl::systemd'
else
  include_recipe 'remctl::inetd'
  include_recipe 'remctl::systemd'
  include_recipe 'remctl::systemv'
end

# Update ACL files
(node['remctl']['acl_files'] || {}).each do |name, acl_entries|
  remctl_acl_file name do
    entries acl_entries
  end
end

# Update command files
(node['remctl']['command_files'] || {}).each do |name, properties|
  remctl_command_file name do
    executable properties['executable']
    subcommands properties['subcommands'] || 'ALL'
    options properties['options']
    acl properties['acl']
  end
end

# Expose executables in directories
(node['remctl']['command_dirs'] || {}).each do |dir, properties|
  remctl_command_dir dir do
    filter properties['filter']
    subcommands properties['subcommands'] || 'ALL'
    options properties['options']
    acl properties['acl']
  end
end

include_recipe 'remctl::clean' if node['remctl']['clean']
