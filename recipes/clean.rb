#
# Cookbook:: remctl
# Recipe:: clean
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Remove unconfigured files
%w(acl conf.d).each do |dir|
  Dir["/etc/remctl/#{dir}/*"].each do |f|
    # Test custom entries in this Chef run
    next if %i(file template).any? do |res_type|
      find_resource(res_type, f)
    end

    # Test custom resources from this cookbook
    res_types = if dir.eql?('acl')
                  %i(remctl_acl_file)
                else
                  %i(remctl_command_file remctl_script_file)
                end
    next if res_types.any? do |res_type|
      find_resource(res_type, File.basename(f))
    end

    # Test remctl_command_dir
    content = ::File.read(f).split(/\r?\n/).reject { |l| l.start_with? '#' }
    executables = content.map(&:split).select { |l| l.length > 3 }.map { |l| l[2] }
    dirnames = executables.map { |l| ::File.dirname(l) }.uniq
    next if dirnames.length == 1 && find_resource(:remctl_command_dir, dirnames.first)

    file f do
      action :delete
      notifies remctl_inetd_restart_action, 'service[inetd]' if remctl_use_inetd?
      notifies :restart, 'service[remctld]' unless remctl_use_inetd?
    end
  end
end
