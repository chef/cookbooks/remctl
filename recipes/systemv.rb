#
# Cookbook:: remctl
# Recipe:: systemv
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

return unless platform_family?('debian')

if remctl_use_systemv?
  edit_resource(:service, 'remctld') do
    supports restart: true
    restart_command '/etc/init.d/remctld restart'
  end

  directory '/etc/init.d'

  cookbook_file '/etc/init.d/remctld' do
    source 'system_v_init_script.sh'
    user 'root'
    group 'root'
    mode '0744'
    backup false
    notifies :restart, 'service[remctld]'
  end
elsif ::File.exist?('/etc/init.d/remctld')
  execute 'stop remctld' do
    # force use of init script and stop cookstyle from complaining
    command('/etc/init.d/remctld ' + 'stop')
  end

  file '/etc/init.d/remctld' do
    action :delete
  end
end
