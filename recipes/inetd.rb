#
# Cookbook:: remctl
# Recipe:: inetd
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

return unless platform_family?('debian')

return if !remctl_use_inetd? && node['packages']['openbsd-inetd'].nil?

package 'openbsd-inetd' if remctl_use_inetd?

edit_resource(:service, 'inetd') do
  action %i(enable start) if remctl_use_inetd?
  supports(reload: true) if remctl_inetd_restart_action == :reload
end

restart_action = remctl_inetd_restart_action
if remctl_use_inetd?
  append_if_no_line 'add remctld to initd' do
    path '/etc/inetd.conf'
    line 'remctl          stream  tcp     nowait  root' \
      '    /usr/sbin/tcpd /usr/sbin/remctld'
    notifies restart_action, 'service[inetd]', :immediately
  end
else
  delete_lines 'remove remctld from initd' do
    path '/etc/inetd.conf'
    pattern '^remctl\s'
    ignore_missing true if respond_to? :ignore_missing
    notifies restart_action, 'service[inetd]', :immediately
  end
end
