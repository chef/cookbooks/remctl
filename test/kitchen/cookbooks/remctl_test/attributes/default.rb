override['remctl']['acl_files'] = {
  test_entries: %w(krb5:member@EXAMPLE.COM),
}
override['remctl']['command_files'] = {
  basename: {
    executable: '/usr/bin/basename',
    subcommands: {
      valid_name: {
        acl: 'file:test_entries',
      },
      invalid_name: {},
    },
    options: 'help=--help',
    acl: 'krb5:admin@EXAMPLE.COM',
  },
}
override['remctl']['command_dirs'] = {
  '/usr/sbin' => {
    filter: '^user',
    acl: 'krb5:admin@EXAMPLE.COM',
  },
}
