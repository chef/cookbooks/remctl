#
# Cookbook:: remctl_test
# Recipe:: krb5
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if platform_family?('debian')
  package 'krb5-kdc'
  package 'krb5-admin-server'
elsif platform_family?('rhel')
  package 'krb5-server'
  package 'krb5-workstation'
end

cookbook_file 'krb5.conf' do
  path '/etc/krb5.conf'
  notifies :restart, 'service[krb5]'
end

directory '/etc/krb5kdc'

file '/etc/krb5kdc/kadm5.acl' do
  content ''
  action :create_if_missing
  notifies :restart, 'service[krb5]'
end

execute 'create db2 database' do
  command 'printf \'master\nmaster\n\' | sudo kdb5_util create -s'
  creates '/var/lib/krb5kdc/principal' # debian
  creates '/var/kerberos/krb5kdc/principal' # rhel
  not_if { ::File.exist? '/etc/krb5kdc/principal' }
  not_if { ::File.exist? '/var/lib/krb5kdc/principal' } # debian
  not_if { ::File.exist? '/var/kerberos/krb5kdc/principal' } # rhel
  notifies :restart, 'service[krb5]'
end

service 'krb5' do
  if platform_family? 'debian'
    service_name 'krb5-kdc'
  elsif platform_family? 'rhel'
    service_name 'krb5kdc'
  end
  action %i(enable start)
end

if platform_family?('debian')
  service 'krb5-admin-server' do
    action %i(enable start)
  end
end

execute 'Add principal vagrant@VAGRANTUP.COM' do
  command 'echo add_principal -pw vagrant vagrant | kadmin.local'
  not_if 'echo list_principals | kadmin.local | grep -q vagrant@VAGRANTUP.COM'
end

execute 'Add principal host/localhost' do
  command 'echo add_principal -randkey host/localhost | kadmin.local'
  not_if 'echo list_principals | kadmin.local | grep -q host/localhost@VAGRANTUP.COM'
end

execute 'Add host principal to keytab' do
  command 'echo ktadd host/localhost | kadmin.local'
  not_if { ::File.exist? '/etc/krb5.keytab' }
end
