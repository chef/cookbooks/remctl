#
# Cookbook:: remctl
# Recipe:: unclean
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Because the following files are created at compile time we create
# directories ourselves which would be created by remctl::server
%w(/etc/remctl /etc/remctl/acl /etc/remctl/conf.d).each do |dir|
  Dir.mkdir(dir) unless Dir.exist?(dir)
end

# Create unconfigured files, which should be removed by remctl::clean
%w(acl conf.d).each do |dir|
  File.open("/etc/remctl/#{dir}/unconfigured", 'w') do |f|
    f.puts 'This is an unconfigured test file'
    f.puts 'It should be deleted by remctl::clean'
  end
end
