#
# Cookbook:: remctl
# Recipe:: default
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include_recipe 'remctl::server'
include_recipe 'remctl_test::krb5'
include_recipe 'remctl_test::unclean'

remctl_script_file 'cookbook_file' do
  source 'test.sh'
  options %w(help=help summary=summary)
  acl 'regex:.*'
end

remctl_script_file 'file' do
  content <<SCRIPT.gsub(/^\s*/, '')
    #!/bin/sh
    case "$1" in
    help) echo help text ;;
    valid) echo valid ;;
    invalid) echo invalid; exit 1 ;;
    summary) echo 'test script (file) for remctl' ;;
    esac
SCRIPT
  options %w(help=help summary=summary)
  acl 'regex:.*'
end

remctl_script_file 'template' do
  source 'test.sh.erb'
  variables(
    resource: 'template'
  )
  options %w(help=help summary=summary)
  acl 'regex:.*'
end

include_recipe 'remctl::clean'
