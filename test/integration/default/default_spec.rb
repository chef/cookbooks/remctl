REMCTLD_PORT = 4373

describe port(REMCTLD_PORT) do
  it { should be_listening }
  # its('addresses') { should include '0.0.0.0' }
  # its('addresses') { should include '::' }
end

if os.family == 'debian'
  describe service('krb5-kdc') do
    it { should be_running }
  end
  describe service('krb5-admin-server') do
    it { should be_running }
  end
elsif os.family == 'redhat'
  describe service('krb5kdc') do
    it { should be_running }
  end
end

describe command('echo list_principals | sudo TERM=screen kadmin.local') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match 'host/localhost@VAGRANTUP.COM' }
  its(:stdout) { should match 'vagrant@VAGRANTUP.COM' }
  its(:stderr) { should be_empty }
end

def remctl_cmd(script, cmd = '')
  "su vagrant -c 'echo vagrant | kinit; remctl localhost #{script} #{cmd}'"
end

describe command(remctl_cmd('help')) do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match 'Password for vagrant@VAGRANTUP.COM:' }
  its(:stdout) { should match(/test script \(cookbook_file\) for remctl/) }
  its(:stdout) { should match(/test script \(file\) for remctl/) }
  its(:stdout) { should match(/test script \(template\) for remctl/) }
  its(:stderr) { should be_empty }
end

%w(cookbook_file file template).each do |script|
  describe command(remctl_cmd(script, 'valid')) do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match 'Password for vagrant@VAGRANTUP.COM:' }
    its(:stdout) { should match 'valid' }
    its(:stderr) { should be_empty }
  end

  describe command(remctl_cmd(script, 'invalid')) do
    its(:exit_status) { should eq 1 }
    its(:stdout) { should match 'Password for vagrant@VAGRANTUP.COM:' }
    its(:stdout) { should match 'invalid' }
    its(:stderr) { should be_empty }
  end
end
