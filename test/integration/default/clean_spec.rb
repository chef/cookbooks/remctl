describe file('/etc/remctl/acl/unconfigured') do
  it { should_not exist }
end

describe file('/etc/remctl/conf.d/unconfigured') do
  it { should_not exist }
end

describe file('/etc/remctl/conf.d/useradd') do
  it { should exist }
end
