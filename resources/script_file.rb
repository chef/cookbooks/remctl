#
# Cookbook:: remctl
# Resource:: remctl_script_file
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true if respond_to? :unified_mode

resource_name 'remctl_script_file'

# (cookbook_)file/template properties
property :atomic_update, [true, false]
property :backup, [false, Integer], default: 5
property :checksum, [String] # only file
property :content, [Array, String] # orders file
property :cookbook, [String] # only cookbook_file and template
property :force_unlink, [true, false], default: false
property :group, [String, Integer], default: 'root'
# property :helper impossible
property :helpers, [Module] # only template
property :inherits, [true, false], default: true
property :local, [true, false], default: false # only template
property :manage_symlink_source, [true, false]
property :mode, [String, Integer], default: '0755'
property :owner, [String, Integer], default: 'root'
property :path, [String]
property :rights, [Hash]
property :source, [String] # only cookbook_file and template
property :variables, [Hash] # orders template
property :verify, [String] # block is impossible

# remctl_command_file properties
property :subcommands, [Array, Hash, String], default: 'ALL'
property :options, [Array, Hash, String]
property :acl, [Array, String]

GENERAL_PROPERTIES = %i(atomic_update backup force_unlink group inherits
                        manage_symlink_source mode owner path rights sensitive
                        verify).freeze
FILE_PROPERTIES = GENERAL_PROPERTIES + %i(checksum content)
COOKBOOK_FILE_PROPERTIES = GENERAL_PROPERTIES + %i(cookbook source)
TEMPLATE_PROPERTIES = GENERAL_PROPERTIES + %i(cookbook helpers local
                                              source variables)

action :create do
  if !new_resource.content.nil?
    file script_file_path do
      FILE_PROPERTIES.each { |prop| set_if_set(self, prop) }
    end
  elsif !new_resource.variables.nil?
    template script_file_path do
      TEMPLATE_PROPERTIES.each { |prop| set_if_set(self, prop) }
    end
  else
    cookbook_file script_file_path do
      COOKBOOK_FILE_PROPERTIES.each { |prop| set_if_set(self, prop) }
    end
  end

  remctl_command_file new_resource.name do
    executable script_file_path
    subcommands new_resource.subcommands
    options new_resource.options
    acl new_resource.acl
  end
end

action :delete do
  file script_file_path do
    action :delete
  end

  remctl_command_file new_resource.name do
    action :delete
  end
end

action_class do
  def coerce_to_string(value)
    value = value.join("\n") if value.is_a? Array
    value
  end

  def script_file_path
    return new_resource.path unless new_resource.path.nil?
    "/usr/local/sbin/remctl-#{new_resource.name}"
  end

  def set_if_set(resource, value)
    return unless resource.respond_to? value

    new_value = new_resource.send(value)
    resource.send(value, new_value) unless new_value.nil?
  end
end
