#
# Cookbook:: remctl
# Resource:: remctl_command_dir
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true if respond_to? :unified_mode

resource_name 'remctl_command_dir'

property :filter, [Regexp, String]

# remctl_command_file properties
property :subcommands, [Array, Hash, String], default: 'ALL'
property :options, [Array, Hash, String]
property :acl, [Array, String]

action :create do
  with_executables do |executable, basename|
    remctl_command_file basename do
      executable executable
      subcommands new_resource.subcommands
      options new_resource.options
      acl new_resource.acl
    end
  end
end

action :delete do
  with_executables do |_, basename|
    file conf_file_path(basename) do
      action :delete
      notifies remctl_inetd_restart_action, 'service[inetd]' if remctl_use_inetd?
      notifies :restart, 'service[remctld]' unless remctl_use_inetd?
    end
  end
end

action_class do
  def conf_file_path(name)
    "/etc/remctl/conf.d/#{name}"
  end

  def with_executables
    dir = new_resource.name
    unless Dir.exist?(dir)
      Chef::Log.warn("remctl command directory #{dir} does not exists")
      return []
    end

    filter = new_resource.filter
    filter = ::Regexp.new(filter) if filter.is_a? String

    Dir[dir.chomp('/') + '/*'].each do |f|
      base = ::File.basename(f)
      next if filter && !filter.match(base)
      next unless ::File.executable? f

      yield(f, base)
    end
  end
end
