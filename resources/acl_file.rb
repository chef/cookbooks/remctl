#
# Cookbook:: remctl
# Resource:: remctl_acl_file
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true if respond_to? :unified_mode

resource_name 'remctl_acl_file'

property :entries, Array, default: []
property :source, String, default: 'acl_file.erb'
property :cookbook, String, default: 'remctl'

action :create do
  inetd_action = remctl_inetd_restart_action
  template "/etc/remctl/acl/#{valid_name(new_resource.name)}" do
    source new_resource.source
    cookbook new_resource.cookbook
    variables(entries: new_resource.entries || [])
    notifies inetd_action, 'service[inetd]' if remctl_use_inetd?
    notifies :restart, 'service[remctld]' unless remctl_use_inetd?
  end
end

action :delete do
  file "/etc/remctl/acl/#{valid_name(new_resource.name)}" do
    action :delete
  end
end

action_class do
  include Remctl::Helper

  def valid_name(name)
    basename = ::File.basename(name)
    Chef::Log.warn "Name for ACL file is not valid: #{name}" if basename != name
    basename
  end
end
