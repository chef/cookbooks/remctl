#
# Cookbook:: remctl
# Resource:: remctl_command_file
#
# Copyright:: 2020-2022, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true if respond_to? :unified_mode

resource_name 'remctl_command_file'

property :executable, [String]
property :subcommands, [Array, Hash, String], default: 'ALL'
property :options, [Array, Hash, String]
property :acl, [Array, String]

action :create do
  options = coerce_to_hash(new_resource.options, '=')
  acl = coerce_to_array(new_resource.acl)
  entries = []
  subcommands_hash.each do |subcommand, props|
    props ||= {}
    entries << {
      command: new_resource.name,
      subcommand: subcommand,
      executable: props['executable'] || props[:executable] || new_resource.executable,
      options: options.merge(coerce_to_hash(props['options'] || props[:options], '=')),
      acl: acl.concat(coerce_to_array(props['acl'] || props[:acl])).uniq,
    }
  end

  template conf_file_path do
    source 'commands_file.erb'
    cookbook 'remctl'
    variables(entries: entries)
    notifies remctl_inetd_restart_action, 'service[inetd]' if remctl_use_inetd?
    notifies :restart, 'service[remctld]' unless remctl_use_inetd?
  end
end

action :delete do
  file conf_file_path do
    action :delete
    notifies remctl_inetd_restart_action, 'service[inetd]' if remctl_use_inetd?
    notifies :restart, 'service[remctld]' unless remctl_use_inetd?
  end
end

action_class do
  def coerce_to_array(value)
    value = value.split if value.is_a? String
    (value || []).uniq
  end

  def coerce_to_hash(value, sep)
    coerce_to_array(value).map { |v| v.split(sep) }.to_h
  end

  def conf_file_path
    "/etc/remctl/conf.d/#{new_resource.name}"
  end

  def subcommands_hash
    subcommands = new_resource.subcommands
    subcommands = subcommands.split if subcommands.is_a? String
    subcommands = subcommands.map { |v| [v, {}] } if subcommands.is_a? Array
    subcommands
  end
end
