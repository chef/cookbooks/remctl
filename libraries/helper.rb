module Remctl
  # Helper functions for remctl cookbook
  module Helper
    module_function

    def remctl_config_path
      if platform?('debian')
        '/etc/remctl/remctl.conf'
      else
        '/etc/remctl.conf'
      end
    end

    def remctl_inetd_restart_action
      if platform?('debian') && node['platform_version'].to_i < 9
        :restart
      else
        :reload
      end
    end

    def remctl_use_inetd?
      node['remctl']['init'].eql?('inetd')
    end

    def remctl_use_systemd?
      node['remctl']['init'].eql?('systemd')
    end

    def remctl_use_systemv?
      node['remctl']['init'].eql?('systemv')
    end
  end
end

Chef::DSL::Recipe.include Remctl::Helper
Chef::Resource.include Remctl::Helper
