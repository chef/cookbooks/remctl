remctl Cookbook
===============

Setup remctl client or server to accept and execute remote commands.

Recipes
-------

* `client`: Install the remctl client.
* `server`: Install the remctl client and server. Includes the following recipes:
* `inetd`, `systemd`, `systemv`: Setup the init system for the daemon.
* `clean`: Delete foreign files (see below).

Configuration
-------------

The remctl daemon can be controlled by inetd, systemd or System-V, depending on the platform. It can be forced with `node['remctl']['init'] = 'inetd|systemd|systemv'`.

Custom configuration files can be listed under `node['remctl']['include_files']`. This does not create any files.

ACL files
---------

ACL files can either be defined in attributes or by using the resource `remctl_acl_file`.

```ruby
# as attributes:
node['remctl']['acl_files'] = {
  <name>: [
    # for valid entries see remctld manpage
    'anyuser:(auth|anonymous)',
    'princ:<principal>',
    'localgroup:<groupname>',
    'regex:<regex>',
    # ...
  ]
}

# using a resource:
remctl_acl_file '<name>' do
  entries [
    # same entries as before
  ]
  # use another template from another cookbook
  source '<name>'
  cookbook '<name>'
end
```

Command files
-------------

Commands are kept in one file per command. They can be defined in attributes or by using the resource `remctl_command_file`.

```ruby
# as attributes:
node['remctl']['command_files'] = {
  <command>: {
    # entries as below
    executable: '<executable>',
    subcommands: ...,
    options: ...,
    acl: ...
  }
}

remctl_command_file '<name>' do
  executable '<executable>',

  # ACL can be a string or array
  acl 'princ:<principle> ...'
  acl ['princ:<principle>', ...]

  # options can be a string, an array or a hash
  options 'help=help ...'
  options ['help=help', ...]
  options { help: 'help', ... }

  # subcommand can be a string, an array or a hash
  subcommands 'ALL' # or any other subcommand
  subcommands ['<subcommand>', ...]
  subcommands {
    <subcommand>: {
      # variables here will overwrite or expand the ones before
      executable: '<executable>',
      options: ...,
      acl: ...
    }
  }
end
```

Command directories
-------------------

The resource `remctl_command_dir` helps if you have a directory from which you want to use multiple executable files. It searches a directory, filters the result and creates `remctl_command_file` resources for every matching executable.

```ruby
# as attributes:
node['remctl']['command_dirs'] = {
  <directory>: {
    # entries as below
    filter: '<ruby regex>',
    subcommands: ...,
    options: ...,
    acl: ...
  }
}

remctl_command_dir '<path>' do
  filter '<ruby regex>', # or a Regexp object
  # all other properties as defined by remctl_command_file
  # "executable" is assigned by remctl_command_dir
end
```

Script files
------------

A script which is dedicated to be used by remctl, can be created with the `remctl_script_file` resource. It is a hybrid between a `file/cookbook_file/template` and a `remctl_command_file` resource.

```ruby
remctl_script_file '<name>' do
  # the content of the script can be given as a string (like file)
  content '<content>'
  # or it can pulled from another cookbook (like cookbook_file)
  source '<source>'
  cookbook '<cookbook>'
  # or by using a erb file (like template)
  variables '<hash>'
  source '<source>'
  # The default path is /usr/local/sbin/remctl-<name>
  path '<path>'
  # The same properties as in file/cookbook_file/template
  mode '0755'
  backup false
  ...
  # the same properties as in remctl_command_file
  subcommands <array, hash or string>
  options <array or string>
  acl <array or string>
end
```

Clean configuration files
-------------------------

The recipe `clean` cleans up the remctl configuration directory. It looks for files which are neither configured through `file`, `template`, `remctl_acl_file`, `remctl_command_file` nor `remctl_script_file` resources. These files are deleted.

Because the recipe looks for already defined resources it is important that it runs at a later time. Preferably add it at the end of a runlist.

If you do not declare resources outside of role definitions or you are certain, that `remctl::server` runs late enough, you can set the attribute `node['remctl']['clean']` to `true`. This runs the recipe `clean` right after `server`.
